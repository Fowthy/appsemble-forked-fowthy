import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: {
    id: 'studio.xmcVZ0',
    defaultMessage: 'Search',
  },
});
